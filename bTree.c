#include <stdio.h>
#include <stdlib.h>
#define TYPE char

#define T 2
#define NOT_FOUND -1
#define TRUE 1
#define FALSE 0


typedef struct _node {
  int n;
  int leaf;
  TYPE keys[2 * T - 1];
  struct _node *child[2 * T];
} Node, BTree;

BTree* createBtree () {
   BTree *a = (BTree *)malloc(sizeof(BTree));
   a->n = 0;
   a->leaf = TRUE;
   return a;
}


void printBtree (BTree *a, int level) {
   int i;


   for (i = 0; i < level; i++) { printf("  "); }


   printf("|");
   for (i = 0; i < a->n; i++) {
      printf("%c|", a->keys[i]);

   }
   printf("\n");


   for (i = 0; i <= a->n; i++) {
      if (a->leaf == FALSE) {
         printBtree(a->child[i], level + 1);
      }
   }
}


BTree* splitNode (BTree *x, int i, BTree *y) {
    int j;
    BTree *z = createBtree();					//tworzymy nowy wezel
    z->n = T - 1;								//liczba kluczy z rowna T-1
	z->leaf = y->leaf;

    for (j = 0; j < T - 1; j++){				//do z podpinamy klucze od srodkowego elementu "y" (j+T) do ostatniego
        z->keys[j] = y->keys[j+T];
    }

    if (y->leaf == FALSE)						//gdy "y" nie jest lisciem
    {
        for (j = 0; j < T; j++){				//do z podpinamy dzieci od srodkowego elementu "y" do ostatniego
            z->child[j] = y->child[j+T];
        }
    }

    y->n = T - 1;								//aktualizujemy liczbe kluczy w "y" po wyjeciu kluczy do "z"

    for (j = x->n; j >= i+1; j--){				//dzieci "x" przesuwamy w prawo - robimy miejsce
        x->child[j+1] = x->child[j];
    }

    x->child[i+1] = z;							//nowym dzieckiem "x" zostaje elem. "z"


    for (j = x->n-1; j >= i; j--){				//klucze x przesuwamy w prawo - robimy miejsce
        x->keys[j+1] = x->keys[j];
    }

    x->keys[i] = y->keys[T-1];					//element srodkowy z "y" przypisujemy do x


    x->n++;										//liczba kluczy w "x" zwiekszona o 1


   return x;
}

BTree* insertNonFull (BTree *x, TYPE k) {
    int i = x->n-1;								//i to indeks ostatniego elementu w tablicy

	//nowy element wstawiamy do wierzcholka bedacego lisciem
    if (x->leaf == TRUE){

        while (i >= 0 && x->keys[i] > k){		//dopooki nowy klucz jest mniejszy od starego
            x->keys[i+1] = x->keys[i];			//przesuwamy tablice w prawo
            i--;
        }


        x->keys[i+1] = k;						//gdy nowy klucz jest wiekszy od starego (nie wchodzimy do while) - umieszczamy nowy klucz za ostatnim istniejacym (i+1)
        										//gdy nowy klucz jest mniejszy od starego (wchodzimy do while;... 
												//...przesuwamy odpowiednia ilosc elementow tablicy w prawo; tworzymy puste pole) - umieszczamy nowy klucz na pustym
												//										 miejscu o indeksie i+1 gdyz wszlismy poza puste miejsce poprzez i--
        x->n++;									//zwiekszamy liczbe kluczy w "x" o 1


    }
    
    //nowy element wstawiamy do wierzcholka ktory nie jest lisciem
    else
    {
        while (i >= 0 && x->keys[i] > k){		//dopooki nowy klucz jest mniejszy od starego
            i--;								//zmniejszamy indeks
        }

        i = i + 1;								//powracamy do wlasciwego indeksu ktory zostal niepotrzebnie odjety w ostatniej iteracji

        if (x->child[i]->n == 2*T-1){			//jesli "dziecko x" jest pelne
            x = splitNode(x, i, x->child[i]);	//wykonujemy funkcje dzielenia "dziecka x"

            if (k > x->keys[i]){			//sprawdzamy czy nowy klucz jest wiekszy od elementu umieszczonego w "x" w wyniku dzielenia "dziecka x"
                i = i + 1;					//jesli tak to zwiekszamy indeks tablicy
            }
        }

        x->child[i] = insertNonFull(x->child[i], k);	//wywolanie calej funkcji rekurencyjnie
    }

    return x;
}


BTree *insert (BTree *root, TYPE keys) {
   BTree *r = root;
   if (r->n == (2*T - 1)) {
      BTree *s = createBtree();
      s->leaf = FALSE;
      s->child[0] = r;
      s = splitNode (s, 0, r);
      s = insertNonFull (s, keys);
      return s;
   }
   else {
      return insertNonFull (r, keys);
   }
}

int search (BTree *x, TYPE k){
		int i = 0;
		while (i<x->n && k>x->keys[i]){
			i++;
		}
		if (i <= x->n && k == x->keys[i]){
			printf ("znalezione");
			return 1;
		}
		if (x->leaf){
			printf ("nie znalezione");			
			return 0;
		}else{
			search (x->child [i], k);
		}
	}

void delete (BTree *x, TYPE k){
		int i = 0;
		int c;
		while (i<x->n && k>x->keys[i]){		//iterujemy po elementach tablicy kluczy "x" gdy nasz element jest wiekszy od aktualnego
		i++;
		}
		//jesli usuwany element znalezlismy wsrod kluczy wezla "x"
		if (k == x->keys[i]){				// element mogl byc mniejszy lub rowny; sprawdzamy czy rowny
			c = i;							//wartosc i-tego elementu tablicy stracimy w iteracji dlatego zapisujemy do c
			//jesli ten element jest lisciem
			if (x->leaf){
				for (i=i+1; i<n; ++i)			//przesuwamy elementy tablicy w lewo tym samym usuwamy wartosc ktora chcielismy
        		x->keys[i-1] = x->keys[i];
       			x->n --;
       		}
       		//jesli ten element nie jest lisciem
       		else{
       			//I.
       			int LKDX1 = x->child[c]->n;		//LKDX to liczba kluczy dziecka "x" (1-po lewej stronie k,
       			int LKDX2 = x->child[c+1]->n;	//									 2 - po prawej stronie k)
				   if (LKDX1 >= T){				//gdy dziecko poprzedzajace element "k" ma co najmniej T kluczy
       					x->keys[c] = x->child[c]->keys[LKDX1 - 1];
       					delete (x->child[c], x->child[c]->keys[LKDX1 - 1]);
				   }else if (LKDX2 >= T){		//gdy dziecko nastepujace po elemencie "k" ma co najmniej T kluczy
				   		x->keys[c] = x->child[c+1]->keys[0];
       					delete (x->child[c+1], x->child[c+1]->keys[0]);
				   }else{
				   	
				   }
			}
		}
		//jesli usuwanego elementu nie ma wsrod kluczy wezla "x"
		else{
			
		}
	}

int main () {

   BTree *a = createBtree();

   a = insert (a, 'F');
   a = insert (a, 'S');
   a = insert (a, 'Q');
   a = insert (a, 'K');
   a = insert (a, 'C');
   a = insert (a, 'L');
   a = insert (a, 'H');
   a = insert (a, 'T');
   a = insert (a, 'V');
   a = insert (a, 'W');
   a = insert (a, 'M');
   a = insert (a, 'R');
   a = insert (a, 'N');
   a = insert (a, 'P');
   a = insert (a, 'A');
   a = insert (a, 'B');
   a = insert (a, 'X');
   a = insert (a, 'Y');
   a = insert (a, 'D');
   a = insert (a, 'Z');
   a = insert (a, 'E');

   printBtree(a, 0);

   a = search (a, 'O');

   return 0;
}
